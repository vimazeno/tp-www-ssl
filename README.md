## How you can get it

```bash
git clone git@gitlab.isima.fr:mazenovi/tp-www-ssl.git && cd tp-www-ssl
```

or

```bash
git clone https://gitlab.isima.fr/mazenovi/tp-www-ssl.git && cd tp-www-ssl
```

## How can you use it?

```bash
vagrant up
```

### Test it

http://0.0.0.0:8080/ -> Apache2 Debian Default Page


## How can you use command line on it?

```bash
vagrant ssh
```

## How can you stop it?

```bash
vagrant halt
```

## How can you reset it?

```bash
vagrant destroy
```

## How can you reexecute provioning script on it?

```bash
vagrant provision
```

* You can provision with provision.sh script

s4uC3
* https://raymii.org/s/
  * ssl-decoder https://github.com/RaymiiOrg/ssl-decoder
* firefox add ons
  * https://addons.mozilla.org/fr/firefox/addon/calomel-ssl-validation/
  * https://addons.mozilla.org/fr/firefox/addon/toggle-cipher-suites/
  * https://addons.mozilla.org/fr/firefox/addon/ssleuth/
* https://blogs.msdn.microsoft.com/benjaminperkins/2014/05/05/make-your-own-ssl-certificate-for-testing-and-learning/

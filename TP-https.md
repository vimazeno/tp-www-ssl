## TP HTTPS


L'objectif du TP est de configurer un site web en http pour qu'il soit accessible en https. L'objectif n'étant pas le designde page web, nous supposons que les pages sont stockées dans le répertoire  ``/var/www/html/index.html``.

Nous utilisons  [git](https://fr.wikipedia.org/wiki/Git) et  [vagrant](https://www.vagrantup.com/) qui utilise [VirtualBox](https://www.virtualbox.org/).

### Installation

```bash
git clone git@gitlab.isima.fr:mazenovi/tp-www-ssl.git && cd tp-www-ssl
```

or

```bash
git clone https://gitlab.isima.fr/mazenovi/tp-www-ssl.git && cd tp-www-ssl
```

Le répertoire créé contient un `README.md`, le fichier de configuration `Vagrantfile`,le script d'installation de la machine `provision.sh` et le sujet du TP (`TP-https.md`).

Le fichier `Vagrantfile` contient les éléements de configuration, en partilculier les ports utilisés, le fichier de configuration ici `provision.sh`. L'image de la machine virtuelle `mazenovi/debian` est stockée sur le site d'[hasicorp.com](https://www.hashicorp.com/atlas.html)

```bash
Vagrant.configure("2") do |config|
  config.vm.box = "mazenovi/debian"
##  config.vm.network "forwarded_port", guest: 80, host: 8080
##  config.vm.network "forwarded_port", guest: 443, host: 8443
  config.vm.provider "virtualbox" do |vb|
     vb.gui = false
     vb.memory = "1024"
  end
  config.vm.provision "shell", path: "provision.sh"
end
```

Les deux lignes commentées permettent de rediriger le port 80 sur le port 8080 et le port 443 sur le port 8443.

## Utilisation de vagrant

Voici les commandes à connaître pour utiliser vagrant.

Pour lancer la machine virtuelle

```bash
vagrant up
```

Pour arrêter la machine virtuelle

```bash
vagrant halt
```

Pour se connecter sur la machine virtuelle

```bash
vagrant ssh
```

La commande suivante permet d'exectuer le script  `provision.sh`.  L'objectif du TP est donc de créer pas à pas votre script qui permet de configurer un site en https.

```bash
vagrant provision
```

Pour détruire la machine virtuelle (au prochain `vagrant up` il faudra recharger l'image de la machine ce qui peut prendre un temps certain en fonction de la vitesse de votre connexion)

```bash
vagrant destroy
```

A noter que le répertoire ``/vagrant/`` permet d'écrire des fichiers sur le disque local qui est en dehors de la machine virtuelle.



### Installation du serveur Apache2 et opensssl

La première ligne du script `provison.sh` doit contenir la commande suivnate de mise à jour des packets.

```bash sudo apt-get update```

Afin d'installer  Apache2 nous rajoutons la commande suivante, où le `y` permet de toujours répondre `yes` à la question de configuration d'apache.

```bash
sudo apt-get -y install apache2
```


#### Vérification que la machine fonctionne

Pour cela il faut ouvrir dans un navigateur la page  `http://0.0.0.0:8080` car le port 8080 a été défini dans la configuration du fichier `Vagrantfile`. Il s'agit de la page intitulée "Apache2 Debian Default Page".

En changeant le fichier `/var/www/html/index.html` vous pouvez vérifier que cela fonctione, en le remplaçant par

```bash
<html>
          It works!
</html>
```

Vérifions que le port https (8443) est bien inactif en accédant à la page suivante `https://0.0.0.0:8443`


# Installation d'openssl

Ajouter la ligne de commande suivante à votre script afin d'installe `openssl`.

```bash
sudo apt-get install openssl
```

Pour prendre en compte ce changement ne pas oublier de faire un `vagrant provision`.

# Création des certificats auto-signés

Par défault openssl fournit un certificat

``/etc/ssl/certs/ssl-cert-snakeoil.pem``

une clef privée

``/etc/ssl/private/ssl-cert-snakeoil.key``


Question 1 : Enregistrer cette clef et ce certificat, puis reintialiser le système. Qu'observez vous avec les nouveaux fichiers?

Question 2 : La commande ci-dessou permet de mieux comprendre ce qui se passe sur le certificat. Quel est le problème avec les deux certificats obtenus?

```bash
openssl x509 -text -in ssl-cert-snakeoil.pem
```


Question 3 :
La commande  suivante permet de regénérer ce certificat et cette clef.

```bash
sudo make-ssl-cert generate-default-snakeoil --force-overwrite
```

Question 4 : Regarder le contenu du fichier
`/usr/sbin/make-ssl-cert` pour déterminer les paramètres utilisés pour générer la clef et le certificat.



# Configuration d'Apache2

Pour les fichiers de configuration d'Apache2 lisez les fichiers suivants :

`/etc/apache2/ports.conf`
port 443   `/etc/apache2/sites-available/default-ssl.conf`
port 80 ``/etc/apache2/sites-available/000-default.conf`

La commande suivante active le module ssl d'Apache2 (a2 = apache2, en = enable, mod = module)

```bash
sudo a2enmod ssl
```

Ce qui est équivalent à la commande suivante `ln -s /etc/apache2/sites-available/000-default.conf  /etc/apache2/sites-enabled/000-default.conf`

La commande suivante active active le virtual host  

```bash
sudo a2ensite default-ssl
```

Elle est équivalente aux deux commandes suivantes:

`ln -s /etc/apache2/mods-available/ssl.conf  /etc/apache2/mods-enabled/ssl.conf`

`ln -s /etc/apache2/mods-available/ssl.load  /etc/apache2/mods-enabled/ssl.load`


La commande suivante relance le serveur Apache2

```bash
sudo systemctl reload apache2.service
```

Question 5 : Qu'observez-vous sur le site `http://0.0.0.0:8443/`? Est-ce normal ?

Question 6 : Observer ce site en https?

Question 7 : Vérifier les détails des certificats avant d'accepter.


# Ne plus utiliser que https


```bash
sudo a2enmod rewrite
```

sudo sed -i 's/<\/VirtualHost>/        RewriteEngine On\n<\/VirtualHost>/' /etc/apache2/sites-enabled/000-default.conf
sudo sed -i 's/<\/VirtualHost>/        RewriteCond %{HTTPS} off\n<\/VirtualHost>/' /etc/apache2/sites-enabled/000-default.conf
sudo sed -i 's/<\/VirtualHost>/        RewriteRule (.*) https:\/\/%{SERVER_NAME}:8443$1 [R,L]\n<\/VirtualHost>/' /etc/apache2/sites-enabled/000-default.conf

```bash
sudo systemctl reload apache2.service```

# STEP 0 - update mirrors
echo "download the package lists from the repositories and "updates" them to get information on the newest versions of packages and their dependencies"
sudo apt-get update

# STEP 1 - install web server
echo "install Apache2"
sudo apt-get -y install apache2

echo "you can now browser http://0.0.0.0:8080/ you will see Apache2 Debian Default Page"

# STEP2 - install ssl
echo "install openssl"
sudo apt-get install openssl

# STEP3 - ssl certs (re)generation
echo "openssl provides dummy key and dummy certificate"
echo " - /etc/ssl/certs/ssl-cert-snakeoil.pem"
echo " - # /etc/ssl/private/ssl-cert-snakeoil.key"
echo "let regenerate it"
sudo make-ssl-cert generate-default-snakeoil --force-overwrite # regenerate ssl cert

# STEP4 - apache configuration
echo "enable mod_ssl ssl apache's module with a2enmod command (debian/ubuntu world)"
echo "a2enmod -> a2 = apache2, en = enable, mod = module"
echo "under the hood apache execute these two commands to enable mod_ssl"
echo " - ln -s /etc/apache2/mods-available/ssl.conf  /etc/apache2/mods-enabled/ssl.conf"
echo " - ln -s /etc/apache2/mods-available/ssl.load  /etc/apache2/mods-enabled/ssl.load"
sudo a2enmod ssl
echo "now we can use ssl with apache to provides https"

echo "enable default ssl apache's virutal host with a2ensite command (debian/ubuntu world)"
echo "a2 = apache2, en = enable, site = virtual host name"
echo "under the hood apache execute this command to enable default ssl vhost"
echo " - ln -s /etc/apache2/sites-available/000-default.conf  /etc/apache2/sites-enabled/000-default.conf"
sudo a2ensite default-ssl

echo "take a look at /etc/apache2/ports.conf there's nothing to change"
echo "take a look at /etc/apache2/sites-available/000-default.conf listen to (local) port 80 there's nothing to change"
echo "take a look at /etc/apache2/sites-available/default-ssl.conf listen to (local) port 443 there's nothing to change"
echo "note that everything in /var/www/html will be available at "

echo "for any changes to take affect you need to restart apache http://0.0.0.0:8080/ or http://0.0.0.0:8443/"
sudo systemctl reload apache2.service

echo "you can now browser http://0.0.0.0:8443/ you will see bad request because you connect with http on https port)"
echo "you can now browser https://0.0.0.0:8443/ you will see NET::ERR_CERT_AUTHORITY_INVALID because you use dummy cert & key"
echo "you can force your browser to accept it"

# STEP 5 - make host accessible with https only
echo "automatic forward port 80 on 443"
echo "enable mod_rewirte ssl apache's module with a2enmod command (debian/ubuntu world)"
echo "a2enmod -> a2 = apache2, en = enable, mod = module"
echo "under the hood apache execute these two commands to enable mod_ssl"
echo " - ln -s /etc/apache2/mods-available/rewrite.conf  /etc/apache2/mods-enabled/rewrite.conf"
echo " - ln -s /etc/apache2/mods-available/rewrite.load  /etc/apache2/mods-enabled/rewrite.load"
sudo a2enmod rewrite
echo "now we can perform http redirection (and more) with apache"

echo "add 3 lines to vhost /etc/apache2/sites-enabled/000-default.conf (port 80) to forward to 443"
echo " RewriteEngine On"
echo " RewriteCond %{HTTPS} off"
echo " RewriteRule (.*) https://%{SERVER_NAME}/%$1 [R,L]"
sudo sed -i 's/<\/VirtualHost>/        RewriteEngine On\n<\/VirtualHost>/' /etc/apache2/sites-enabled/000-default.conf
sudo sed -i 's/<\/VirtualHost>/        RewriteCond %{HTTPS} off\n<\/VirtualHost>/' /etc/apache2/sites-enabled/000-default.conf
sudo sed -i 's/<\/VirtualHost>/        RewriteRule (.*) https:\/\/%{SERVER_NAME}:8443$1 [R,L]\n<\/VirtualHost>/' /etc/apache2/sites-enabled/000-default.conf

echo "for any changes to take affect you need to restart apache"
sudo systemctl reload apache2.service

# STEP 6 - install ssl benchmark
echo "install ssl-decoder to benchmark (https://github.com/RaymiiOrg/ssl-decoder)"
echo "install some system dependencies"
#libjpeg62-turbo-dev
sudo apt-get install -y libfreetype6-dev libmcrypt-dev \
  libpng12-dev zlib1g-dev libicu-dev g++ python2.7 python-all-dev \
  python-netaddr perl dnsutils wget curl git
#sudo apt-get -y install php5 php5-intl php5-mcrypt php5-gd php5-json php5-curl
sudo apt-get -y install php php-intl php-mcrypt php-gd php-json php-curl

echo "for any changes to take affect you need to restart apache"
sudo apache2ctl restart

echo "create https://0.0.0.0/info.php to see phpinfo page (ensure php is correctly configured)"
echo '<?php phpinfo();' | sudo tee --append /var/www/html/info.php

echo "git clone ssl-decoder in /vagrant and alias it to response to https://0.0.0.0/ssl"
sudo rm -rf /vagrant/ssl-decoder
sudo git clone https://github.com/RaymiiOrg/ssl-decoder.git /vagrant/ssl-decoder
sudo rm -rf /var/www/html/ssl
sudo  ln -s  /vagrant/ssl-decoder /var/www/html/ssl
sudo chown www-data /var/www/html/ssl/results/

echo "you can check your ssl configuration on https://127.0.0.1:8443/ssl/?host=127.0.0.1&port=443&fastcheck=0"
echo "you should see 3 warnings!"
echo "(1) - TLS_FALLBACK_SCSV unsupported. Please upgrade OpenSSL to enable. This offers downgrade attack protection."
echo "      https://community.qualys.com/thread/14996"
echo "      echo 'SSLSessionTickets Off' | sudo tee --append /etc/apache2/apache2.conf"
echo "      Requires Apache >= 2.4.12 and we have Apache/2.4.10 (Debian): sudo apache2ctl -v"
echo "      upgrade apache2 (not covered)"
echo "(2) - HTTP Strict Transport Security not set."
echo "(3) - OCSP Stapling not enabled."
echo "      depend on CA https://www.digicert.com/ssl-support/apache-enable-ocsp-stapling-on-server.htm"

# STEP 7 - enhance apache secuirty configuration
echo "Trying to resolve (2) - HTTP Strict Transport Security not set"
echo "see https://cipherli.st/ force ssl configuration good practices"
echo "see https://raymii.org/s/tutorials/Strong_SSL_Security_On_Apache2.html"
echo 'SSLCipherSuite EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH' | sudo tee --append /etc/apache2/apache2.conf
echo 'SSLHonorCipherOrder On' | sudo tee --append /etc/apache2/apache2.conf

echo "see https://addons.mozilla.org/fr/firefox/addon/toggle-cipher-suites/"
echo 'SSLProtocol All -SSLv2 -SSLv3' | sudo tee --append /etc/apache2/apache2.conf

echo "see https://raymii.org/s/tutorials/Strong_SSL_Security_On_Apache2.html#SSLv2_and_SSLv3"
echo "useless for our apache version"

echo "enable mod_headers apache's module with a2enmod command (debian/ubuntu world)"
echo "a2enmod -> a2 = apache2, en = enable, mod = module"
echo "under the hood apache execute these two commands to enable mod_ssl"
echo " - ln -s /etc/apache2/mods-available/headers.load  /etc/apache2/mods-enabled/headers.load"
sudo a2enmod headers
echo "now we can tweak http headers sent by apache"

echo "restart apache to take modifcations"
sudo apache2ctl restart

echo "see http://blog.adin.pro/2013-09-09/invalid-command-header-perhaps-misspelled-or-defined-by-a-module-not-included-in-the-server-configuration/"
echo "(2) - HTTP Strict Transport Security not set. -> disappeared"
echo "see also https://en.wikipedia.org/wiki/HTTP_Strict_Transport_Security"
echo 'Header always set Strict-Transport-Security "max-age=63072000; includeSubDomains; preload"' | sudo tee --append /etc/apache2/apache2.conf

echo "see https://en.wikipedia.org/wiki/List_of_HTTP_header_fields#Frame-Options#Frame-Options"
echo 'Header always set X-Frame-Options DENY' | sudo tee --append /etc/apache2/apache2.conf

echo "see https://en.wikipedia.org/wiki/List_of_HTTP_header_fields#cite_ref-45"
echo 'Header always set X-Content-Type-Options nosniff' | sudo tee --append /etc/apache2/apache2.conf

echo "see https://raymii.org/s/tutorials/Strong_SSL_Security_On_Apache2.html#SSL_Compression_(CRIME_attack)"
echo 'SSLCompression off' | sudo tee --append /etc/apache2/apache2.conf

echo "see https://raymii.org/s/tutorials/Strong_SSL_Security_On_Apache2.html#OCSP_Stapling"
echo 'SSLUseStapling on' | sudo tee --append /etc/apache2/apache2.conf
echo 'SSLStaplingCache "shmcb:logs/stapling-cache(150000)"' | sudo tee --append /etc/apache2/apache2.conf

echo "restart apache to take modifcations"
sudo apache2ctl restart

# STEP 8 - signed cert and import certificate authority
echo "enhance our key and cert"
echo "see https://blog.didierstevens.com/2008/12/30/howto-make-your-own-cert-with-openssl/"

echo "generate 4096-bit long RSA key for our root CA  (rootca.key)"
openssl genrsa -out /vagrant/cert/rootca.key 4096

echo "self-signed root CA certificate; you’ll need to provide an identity for your root CA (rootca.crt)"
openssl req -new -x509 -days 1826 -key /vagrant/cert/rootca.key -out /vagrant/cert/rootca.crt  -subj '/CN=www.rootdom.com/O=My Root Company Name LTD./C=US'

echo "create a subordinate CA that will be used for the actual signing. First, generate the key (inter.key)"
openssl genrsa -out /vagrant/cert/inter.key 4096

echo "Then, request a certificate for this subordinate CA (inter.csr)"
openssl req -new -key /vagrant/cert/inter.key -out /vagrant/cert/inter.csr -subj '/CN=www.mydom.com/O=My Company Name LTD./C=US'

echo "process the request for the subordinate CA certificate and get it signed by the root CA (inter.crt)"
echo "for error "Error self signed certificate getting chain." see http://stackoverflow.com/questions/23156911/wso2-enterprise-mobility-manager-error-self-signed-certificate-getting-chain#answer-23158993"
openssl x509 -req -days 730 -in /vagrant/cert/inter.csr -CA /vagrant/cert/rootca.crt -CAkey /vagrant/cert/rootca.key -set_serial 01 -out /vagrant/cert/inter.crt

echo "package the keys and certs in a PKCS12 file (inter.p12)"
openssl pkcs12 -export -out /vagrant/cert/inter.p12 -inkey /vagrant/cert/inter.key -in /vagrant/cert/inter.crt -chain -CAfile /vagrant/cert/rootca.crt -password pass:

echo "export keys and certs in pem files (my.key.pem & my.crt.pem)"
openssl pkcs12 -in /vagrant/cert/inter.p12 -out /vagrant/cert/my.key.pem -nocerts -nodes -password pass:
openssl pkcs12 -in /vagrant/cert/inter.p12 -out /vagrant/cert/my.crt.pem -clcerts -nokeys -password pass:

echo "copy keys and certs in /etc/ssl/private & /etc/ssl/certs"
sudo cp /vagrant/cert/my.key.pem /etc/ssl/private/
sudo cp /vagrant/cert/my.crt.pem /etc/ssl/certs/

echo "update ssl-vhost to use new keys and certs"
sudo sed -i 's/ssl-cert-snakeoil.pem/my.crt.pem/' /etc/apache2/sites-enabled/default-ssl.conf
sudo sed -i 's/ssl-cert-snakeoil.key/my.key.pem/' /etc/apache2/sites-enabled/default-ssl.conf

echo "restart apache to take modifcations"
sudo systemctl reload apache2.service

echo "import rootca.crt in your browser ... do you know that you truster all these CA?"
echo "browse https://0.0.0.0:8443/ -> cert doesn't match domain name in url"
echo "you can fake www.mydom.com in our local resolver"
echo "type \"sudo cat '0.0.0.0  www.mydom.com' > /etc/hosts\" on your local machine"
echo "browse https://www.mydom.com:8443/ \o/"
echo "browse https://0.0.0.0:8443/ssl/?host=127.0.0.1&port=&csr=&s= -> Validating certificate chain failed. Probably non-trusted root/self signed certificate, or the chain order is wrong."

# OPTIONAL
# STEP 9 - nginx as reverse proxy / http router - nginx front serve listen 80
# STEP 10 - nginx as reverse proxy / https router - nginx front serve listen 443 - configure cert ssl with nginx
# STEP 11 - make host accessible with https only - 80 closed or forawarded + make apache unrachable from outside
# STEP 12 - enhance nginx secuirty configuration https://cipherli.st/
